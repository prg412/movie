package com.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by jpineda on 10/20/16.
 */
@Controller
public class IndexController
{
    @RequestMapping("/")
    String index(){
        return "index";
    }
}
